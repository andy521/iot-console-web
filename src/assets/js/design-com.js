export class compomentCreator {
  getTemplate = (name) => {
    return $($('#compomentTemplate ._' + name)[0].outerHTML);
  }
  bgcolor = (parent, data) => {
    let com = this.createCom('bgcolor', '背景色', parent, data);
    let children = data.children;
    if (!children) {
      return;
    }

    //添加子元素
    children.forEach(child => {
      let creator = this[child.name];
      creator(com, child)
    })
  }
  layout = (parent, data) => {
    let children = data.children;
    children = children || [{}, {}];
    let com = this.createCom('layout', '布局', parent, data);
    com.removeClass('row-cols-2').addClass('row-cols-' + children.length);

    let creator = this['cell'];
    children.forEach(child => {
      creator(com, child)
    });
  }
  cell = (parent, data) => {
    let children = data.children;
    let com = this.createCom('cell', '单元格', parent, data);
    if (!children) {
      return;
    }

    children.forEach(child => {
      let creator = this[child.name];
      creator(com, child)
    });
  }
  button = (parent, data) => {
    let com = this.createCom('button', '按钮', parent, data);
    com.text(data.props.text || '按钮');
  }
  switch = (parent, data) => {
    let com = this.createCom('switch', '开关', parent, data);
  }
  text = (parent, data) => {
    let com = this.createCom('text', '文本', parent, data);
    com.text(data.props.text || '文本');
  }
  getId = (name) => {
    return (name + "_" + Math.random()).replace('.', '');
  }
  createCom = (name, desc, parent, data) => {
    let id = this.getId(name);
    data = data || {};
    data.id = id;
    data.name = name;
    data.desc = desc;
    let com = this.getTemplate(name);
    if (!data.props) {
      data.props = {};
    }
    if (!data.appearance) {
      data.appearance = {};
    }
    if (!data.binds) {
      data.binds = {};
    }
    if (!data.action) {
      data.action = {};
    }
    com.data('com_data', data);
    com.attr('id', id);
    parent.append(com);
    return com;
  }
}

export class compomentDecode {
  layout = (el) => {
    let data = el.data('com_data');
    data = data || {
      props: {}
    };
    let com = new WgLayout(data.props, data.appearance, data.binds, data.action);
    //cell
    el.find('>._container').each(function() {
      let decode = _compomentDecode['cell'];
      let cell = decode($(this));
      com.add(cell);
    });
    return com;
  }

  cell = (el) => {
    let data = el.data('com_data');
    data = data || {
      props: {}
    };
    let com = new WgCell(data.props, data.appearance, data.binds, data.action);
    //cell
    this.decodeChildren(com, el);
    return com;
  }

  bgcolor = (el) => {
    let data = el.data('com_data');
    data = data || {
      props: {}
    };
    let com = new WgBgColor(data.props, data.appearance, data.binds, data.action);
    //cell
    this.decodeChildren(com, el);
    return com;
  }

  text = (el) => {
    let data = el.data('com_data');
    data = data || {
      props: {}
    };
    return new WgText(data.props, data.appearance, data.binds, data.action);
  }

  button = (el) => {
    let data = el.data('com_data');
    data = data || {
      props: {}
    };
    return new WgButton(data.props, data.appearance, data.binds, data.action);
  }

  switch = (el) => {
    let data = el.data('com_data');
    data = data || {
      props: {}
    };
    return new WgSwitch(data.props, data.appearance, data.binds, data.action);
  }

  decodeChildren = (parent, el) => {
    el.children('._compoment').each(function() {
      let comEl = $(this);
      let clz = comEl.attr('class').replace(/\s+/g, '_').split(/_/g);
      for (var i in clz) {
        let decode = _compomentDecode[clz[i]];
        if (decode) {
          parent.add(decode(comEl));
        }
      }
    });
  }
}
var _compomentDecode = new compomentDecode();

export class compomentUpdate {

  getClsAttr = () => {
    return $('#emptyCls')[0].outerHTML.split(/\s+/)[1].replace(/=.*/, '');
  }
  layout = (el, data) => {
    let clsAttr = this.getClsAttr();
    let nowSize = parseInt(data.props.colSize);
    let oldSize = el.children('._container').length;
    let size = nowSize - oldSize;
    el.removeClass('row-cols-' + oldSize);
    if (size > 0) {
      while (size > 0) {
        el.append('<div ' + clsAttr + ' class="col _container"></div>')
        size--;
      }
    } else {
      while (size < 0) {
        el.children('._container:last').remove();
        size++;
      }
    }
    el.addClass('row-cols-' + nowSize);
    data.props.colSize = nowSize;
    this.saveData(el, data);
  }
  text = (el, data) => {
    el.text(data.props.text);
    this.saveData(el, data);
  }
  button = (el, data) => {
    el.text(data.props.text);
    this.saveData(el, data);
  }
  switch = (el, data) => {
    this.saveData(el, data);
  }

  saveData = (el, data) => {
    el.data('com_data', data);
  }
}

export class Wiget {
  constructor(prop) {
    this.name = prop.name || '';
    this.id = (prop.name + '_' + Math.random());
    this.props = prop;
    this.children = [];
  }

  add = (wg) => {
    this.children.push(wg);
  }

  //展示组件属性
  showProperty = () => {

  }
}

export class WgCell extends Wiget {
  constructor(prop, appearance, binds, action) {
    prop.name = 'cell';
    super(prop);
    this.appearance = appearance;
    this.binds = binds;
    this.action = action;
  }
}

export class WgBgColor extends Wiget {
  constructor(prop, appearance, binds, action) {
    prop.name = 'bgcolor';
    super(prop);
    this.desc = '背景色';
    this.appearance = appearance;
    this.binds = binds;
    this.action = action;
  }
}

export class WgButton extends Wiget {
  constructor(prop, appearance, binds, action) {
    prop.name = 'button';
    super(prop);
    this.desc = '按钮';
    this.appearance = appearance;
    this.binds = binds;
    this.action = action;
  }
}

export class WgSwitch extends Wiget {
  constructor(prop, appearance, binds, action) {
    prop.name = 'switch';
    super(prop);
    this.desc = '开关';
    this.appearance = appearance;
    this.binds = binds;
    this.action = action;
  }
}

export class WgText extends Wiget {
  constructor(prop, appearance, binds, action) {
    prop.name = 'text';
    super(prop);
    this.desc = '文本';
    this.appearance = appearance;
    this.binds = binds;
    this.action = action;
  }
}

export class WgLayout extends Wiget {
  constructor(prop, appearance, binds, action) {
    prop.name = prop.name || 'layout';
    super(prop);
    this.desc = '布局';
    this.colSize = prop.colSize || 1;
    this.appearance = appearance;
    this.binds = binds;
    this.action = action;
  }

}
